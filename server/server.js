var formidable = require('formidable'),
    http = require('http'),
    sys = require('sys'),
    express = require('express'),
    dbox = require("dbox");


var client = dbox.createClient({
  app_key    : "5s5xau1fe4znelh",             // required
  app_secret : "tlyefbk9wi8j7nb",           // required
  root       : "sandbox"            // optional (defaults to sandbox)
})


var options = {
  oauth_token        : "",  
  oauth_token_secret : "", 
}

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "http://localhost:9294");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

var app = express.createServer();

app.configure(function() {
  //app.use(express.bodyParser());
  //app.use(express.cookieParser());
  //app.use(express.session({ secret: 'cool beans' }));
  //app.use(express.methodOverride());
  app.use(allowCrossDomain);
  //app.use(app.router);
  //app.use(express.static(__dirname + '/public'));
});

app.post('/files', function(req, res){
  var form = new formidable.IncomingForm();
  form.uploadDir = './uploads/';
  form.keepExtensions = true;

  form.parse(req, function(err, fields, files) {
    //res.end(sys.inspect({fields: fields, files: files}));
    //console.log(sys.inspect({fields: fields, files: files}));
  });
  
  form.on('file', function(name, file) {
    console.log(sys.inspect(name), sys.inspect(file))


    client.put("deskname/"+name, "here is some text", options, function(status, reply){
      console.log(status)
      // 200
      console.log(reply)
      // {
      //   "size": "225.4KB",
      //   "rev": "35e97029684fe",
      //   "thumb_exists": false,
      //   "bytes": 230783,
      //   "modified": "Tue, 19 Jul 2011 21:55:38 +0000",
      //   "path": "/foo/hello.txt",
      //   "is_dir": false,
      //   "icon": "page_white_text",
      //   "root": "sandbox",
      //   "mime_type": "text/plain",
      //   "revision": 220823
      // }     
    })


    res.json({id: '123', name: file.name, size: file.size, type: file.type});
  });

  /*
  form.onPart = function(part) {
    console.log(sys.inspect(part));
    //part.addListener('data', function() {});
  }
  */
});

app.get('/sync-with-db', function(req, res) {

  client.request_token(function(status, reply){
    console.log(status)
    // 200

    console.log(reply)
    options.oauth_token = reply.oauth_token;
    options.oauth_token_secret = reply.oauth_token_secret;

    res.redirect(client.build_authorize_url(reply.oauth_token, 'http://localhost:3000/access'));
  });
});

app.get('/access', function(req,res) {

  console.log('Access Token request');

  client.access_token(options, function(status, reply){
    console.log(status)
    // 200

    console.log(reply)
    options.oauth_token = reply.oauth_token;
    options.oauth_token_secret = reply.oauth_token_secret;

    res.redirect('/account');
  });

});

app.get('/account', function(req,res) {
  client.account(options, function(status, reply) {
    console.log(status)
    // 200
    console.log(reply) 
    res.json(reply);
  });
});

app.listen(3000);
console.log("ShareDesk server listening on port %d", 3000);


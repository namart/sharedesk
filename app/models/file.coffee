Spine = require('spine')

class File extends Spine.Model
  @configure 'File', 'name', 'type', 'size', 'file'

  #@extend @Local
  @extend Spine.Model.Ajax
  @extend Spine.Model.Uploader
  Spine.Model.host = "http://localhost:3000"

  module.exports = File

require('lib/setup')

Spine = require('spine')
File = require('models/file')
FileList = require('controllers/files.list')
Dropper = require('controllers/dropper')

class App extends Spine.Controller
  constructor: ->
    super

    @dropArea = new Dropper
    @append @dropArea

    @file = new FileList
    @append @file

    #console.dir(Spine.Model)
    #console.dir(File)
    #console.dir(f)
    ###
    File.refresh([
      {id: 1, name: "filename1", type: "jpg", size: "2342", file: {}}
      {id: 2, name: "filename2", type: "jpg", size: "2342", file: {}}
      {id: 3, name: "filename3", type: "jpg", size: "2342", file: {}}
      {id: 4, name: "filename4", type: "jpg", size: "2342", file: {}}
    ])
    ###


module.exports = App
    

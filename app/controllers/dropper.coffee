Spine = require('spine')
File = require('models/file')

class Dropper extends Spine.Controller

  events:
    "dragover": "handleDragOver"
    "drop": "handleDrop"

  className: 'drop-area'

  constructor: ->
    super

  handleDragOver: (e) ->
    e.stopPropagation()
    e.preventDefault()
    #In jQuery events the dataTransfer object is in originalEvent
    e.originalEvent.dataTransfer.dropEffect = 'copy'

  handleDrop: (e) ->
    e.stopPropagation()
    e.preventDefault()
    File.createUpload(e.originalEvent.dataTransfer.files)

module.exports = Dropper
